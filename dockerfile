FROM python:3.8-slim

# Get Debian packages for RTree/Geopandas, GDAL, openCV and gcc
RUN apt-get -yq update \
    && apt-get install -yq libspatialindex-dev \
    && apt-get install -yq gdal-bin libgdal-dev \
    && apt-get install -yq libgl1 \
    && apt-get install -yq build-essential

# Cleaning out the apt-get repository
RUN rm -rf /var/lib/apt/lists/* \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
    && rm -rf /var/cache/apt/

COPY requirements-dev.txt .
COPY requirements.txt .

# Tuning Python/pip environment variables, so as to keep the Docker light
ENV LANG=C.UTF-8 \
PYTHONDONTWRITEBYTECODE=1 \
PYTHONUNBUFFERED=1 \
PYTHONFAULTHANDLER=1 \
PYTHONHASHSEED=random \
PIP_NO_CACHE_DIR=off \
PIP_DISABLE_PIP_VERSION_CHECK=on \
PIP_DEFAULT_TIMEOUT=100

RUN python -m pip install --upgrade pip \
   && python -m pip install -r requirements.txt \
   && python -m pip install -r requirements-dev.txt \
   && python -m pip install --global-option=build_ext --global-option="-I/usr/include/gdal" GDAL==`gdal-config --version`
